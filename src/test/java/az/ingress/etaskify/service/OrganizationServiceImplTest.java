package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.dto.SignUpRequestDto;
import az.ingress.etaskify.dto.SignUpResponseDto;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.mapper.OrganizationMapper;
import az.ingress.etaskify.mapper.OrganizationResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.RoleRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrganizationServiceImplTest {

    @InjectMocks
    private OrganizationServiceImpl organizationService;
    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private OrganizationMapper organizationMapper;
    @Mock
    private OrganizationResponseMapper organizationResponseMapper;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    private Organization organization;
    private OrganizationDto organizationDto;

    private List<Organization> organizationList;

    private List<OrganizationResponseDto> organizationResponseDtoList;

    private List<Organization> organizationEmptyList;


    private SignUpRequestDto signUpRequestDto;

    private Role role;

    private User user;

    private String bearerToken;

    private Integer organizationId;



    @BeforeEach
    public void setUp() {
        signUpRequestDto = SignUpRequestDto.builder().
                name("PASHA Bank").
                phone("+994512296643").
                address("Neftchilar pr. 143").
                username("pasha_admin").
                password(passwordEncoder.encode("123456")).
                email("huseyn.quliyev89@gmail.com").build();

        role = Role.builder()
                .id(1)
                .name(UserRole.ROLE_ADMIN)
                .build();

        user = User.builder()
                .id(1)
                .username("pasha_admin")
                .password(passwordEncoder.encode("123456")).
                email("huseyn.quliyev89@gmail.com")
                .roles(List.of(role))
                .organization(organization).build();


        organization = Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationDto = OrganizationDto.builder()
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationList = Arrays.asList(Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build(),
                Organization.builder()
                        .id(2)
                        .name("ABB Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build());

        organizationResponseDtoList = Arrays.asList(OrganizationResponseDto.builder()
                        .id(1).name("PASHA Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build(),
                OrganizationResponseDto.builder()
                        .id(2).name("ABB Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build());

        organizationEmptyList = Collections.emptyList();

         bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

         organizationId = 1;
    }

    @Test
    void givenValidOrganizationId_whenGetOrganizationById_thenReturnOrganizationDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.of(organization));
        when(organizationMapper.mapToOrganizationDto(any())).thenReturn(organizationDto);

        // act
        OrganizationDto returnOrganization = organizationService.getOrganizationById(bearerToken);

        // assert
        assertThat(returnOrganization).isNotNull();
        assertThat(returnOrganization.getName()).isEqualTo("PASHA Bank");
        assertThat(returnOrganization.getPhone()).isEqualTo("+994512296643");
        assertThat(returnOrganization.getAddress()).isEqualTo("Neftchilar pr. 143");
        verify(organizationRepository, times(1)).findById(1);
    }

    @Test
    void givenInvalidOrganizationId_whenGetOrganizationById_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> organizationService.getOrganizationById(bearerToken)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> organizationService.getOrganizationById(bearerToken));
    }

//    @Test
//    void givenEmptyParameter_whenGetAllOrganizations_thenReturnOrganizationDtoList() {
//        // arrange
//        when(organizationRepository.findAll()).thenReturn(organizationList);
//        when(organizationResponseMapper.mapToOrganizationResponseDtoList(any())).thenReturn(organizationResponseDtoList);
//
//        // act
//        List<OrganizationResponseDto> resultOrganizationList = organizationService.getAllOrganizations();
//
//        // assert
//        assertThat(resultOrganizationList).isNotNull().hasSize(2);
//        assertThat(resultOrganizationList.get(0).getName()).isEqualTo("PASHA Bank");
//        assertThat(resultOrganizationList.get(1).getName()).isEqualTo("ABB Bank");
//    }

//    @Test
//    void givenEmptyParameter_whenGetAllOrganizations_thenReturnEmptyList() {
//        // arrange
//        when(organizationRepository.findAll()).thenReturn(organizationEmptyList);
//
//        // act
//        List<OrganizationResponseDto> resultOrganizationList = organizationService.getAllOrganizations();
//
//        // assert
//        assertThat(resultOrganizationList).isEmpty();
//    }

    @Test
    void givenValidOrganizationIdAndValidOrganizationDto_whenUpdateOrganization_thenReturnOrganizationDto() {
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.of(organization));
        when(organizationMapper.mapToOrganization(any())).thenReturn(organization);
        when(organizationRepository.save(any())).thenReturn(organization);
        when(organizationMapper.mapToOrganizationDto(any())).thenReturn(organizationDto);

        // act
        OrganizationDto savedOrganization = organizationService.updateOrganization(bearerToken, organizationDto);

        // assert
        assertThat(savedOrganization.getName()).isEqualTo("PASHA Bank");
        assertThat(savedOrganization.getPhone()).isEqualTo("+994512296643");
    }

    @Test
    void givenInvalidOrganizationIdAndValidOrganizationDto_whenUpdateOrganization_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> organizationService.updateOrganization(bearerToken, organizationDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> organizationService.updateOrganization(bearerToken, organizationDto));
    }

    @Test
    void givenValidOrganizationId_whenDeleteOrganization_thenNothing() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.ofNullable(organization));
        doNothing().when(organizationRepository).deleteById(anyInt());

        // act
        organizationService.deleteOrganization(bearerToken);

        // assert
        verify(organizationRepository, times(1)).deleteById(1);
    }

    @Test
    void givenInvalidOrganizationId_whenDeleteOrganization_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> organizationService.deleteOrganization(bearerToken)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> organizationService.deleteOrganization(bearerToken));
    }

    @Test
    void givenValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnSignUpResponse() {
        // arrange
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(organizationRepository.findByName(anyString())).thenReturn(Optional.empty());
        when(roleRepository.findByName(UserRole.ROLE_ADMIN)).thenReturn(Optional.of(role));
        when(organizationRepository.save(any())).thenReturn(organization);
        when(userRepository.save(any())).thenReturn(user);

        // act
        SignUpResponseDto resultSignUpResponseDto = organizationService.saveOrganizationAndUser(signUpRequestDto);

        // assert
        assertThat(resultSignUpResponseDto).isNotNull();
        assertThat(resultSignUpResponseDto.getName()).isEqualTo("PASHA Bank");
        assertThat(resultSignUpResponseDto.getPhone()).isEqualTo("+994512296643");
        assertThat(resultSignUpResponseDto.getAddress()).isEqualTo("Neftchilar pr. 143");
        assertThat(resultSignUpResponseDto.getUsername()).isEqualTo("pasha_admin");
        assertThat(resultSignUpResponseDto.getEmail()).isEqualTo("huseyn.quliyev89@gmail.com");
        verify(organizationRepository, times(1)).findByName("PASHA Bank");
        verify(userRepository, times(1)).findByUsername("pasha_admin");


    }

    @Test
    void givenInValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnExistingUserException() {
        // arrange
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

        // act & assert
        assertThatThrownBy(() -> organizationService.saveOrganizationAndUser(signUpRequestDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> organizationService.saveOrganizationAndUser(signUpRequestDto));
    }

    @Test
    void givenInValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnExistingOrganizationException() {
        // arrange
        when(organizationRepository.findByName(anyString())).thenReturn(Optional.of(organization));

        // act & assert
        assertThatThrownBy(() -> organizationService.saveOrganizationAndUser(signUpRequestDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> organizationService.saveOrganizationAndUser(signUpRequestDto));
    }

}
