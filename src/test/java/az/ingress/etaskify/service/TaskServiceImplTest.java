package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.*;
import az.ingress.etaskify.enums.TaskStatus;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.mail.EmailService;
import az.ingress.etaskify.mapper.TaskMapper;
import az.ingress.etaskify.mapper.TaskResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.Task;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.TaskRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TaskMapper taskMapper;

    @Mock
    private TaskResponseMapper taskResponseMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private EmailService emailService;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    private Task task;

    private Task task2;

    private Task taskDb;


    private Task savedTask;

    private List<Task> taskList;

    private TaskDto taskDto;


    private Organization organization;

    private User user;

    private User user2;

    private List<User> userList;

    private Role role;

    private RoleDto roleDto;

    private TaskResponseDto taskResponseDto;

    private TaskResponseDto taskResponseDto2;

    private List<TaskResponseDto> taskResponseDtoList;

    private OrganizationDto organizationDto;

    private UserResponseDto userResponseDto;

    private UserResponseDto userResponseDto2;
    private List<UserResponseDto> userResponseDtoList;

    private String bearerToken;

    private Integer organizationId;


    @BeforeEach
    public void setUp() {
        organization = Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationDto = OrganizationDto.builder()
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        role = Role.builder()
                .id(1)
                .name(UserRole.ROLE_ADMIN)
                .build();

        roleDto = RoleDto.builder()
                .name(UserRole.ROLE_ADMIN)
                .build();

        user = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        user2 = User.builder()
                .id(2)
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("mikayil.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        userList = Arrays.asList(user, user2);

        userResponseDto = UserResponseDto.builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .email("huseyn.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDto2 = UserResponseDto.builder()
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .email("mikayil.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDtoList = Arrays.asList(userResponseDto, userResponseDto2);

        task = Task.builder()
                .id(1)
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organization)
                .assignedUsers(userList)
                .build();

        task2 = Task.builder()
                .id(2)
                .title("CC")
                .description("DD")
                .deadline(LocalDate.of(2024, 3, 28))
                .status(TaskStatus.TODO)
                .organization(organization)
                .assignedUsers(userList)
                .build();

        taskDb = Task.builder()
                .id(1)
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organization)
                .assignedUsers(userList)
                .build();


        savedTask = Task.builder()
                .id(1)
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organization)
                .assignedUsers(userList)
                .build();

        taskDto = TaskDto.builder()
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .assignedUsers(userList)
                .build();


        taskList = Arrays.asList(task, task2);

        taskResponseDto = TaskResponseDto.builder()
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organizationDto)
                .assignedUsers(userResponseDtoList)
                .build();

        taskResponseDto2 = TaskResponseDto.builder()
                .title("CC")
                .description("DD")
                .deadline(LocalDate.of(2024, 3, 28))
                .status(TaskStatus.TODO)
                .organization(organizationDto)
                .assignedUsers(userResponseDtoList)
                .build();

        taskResponseDtoList = Arrays.asList(taskResponseDto, taskResponseDto2);

        bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

        organizationId = 1;

    }

    @Test
    void givenValidOrganizationIdAndTaskId_whenGetTaskById_thenReturnTaskResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.ofNullable(task));
        when(taskResponseMapper.mapToTaskResponseDto(any())).thenReturn(taskResponseDto);

        // act
        TaskResponseDto returnTaskResponseDto = taskService.getTaskById(bearerToken, 1);

        // assert
        assertThat(returnTaskResponseDto).isNotNull();
        assertThat(returnTaskResponseDto.getTitle()).isEqualTo("AA");
        assertThat(returnTaskResponseDto.getDescription()).isEqualTo("BB");
        verify(taskRepository, times(1)).findByIdAndOrganizationId(1,1);
    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenGetTaskById_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> taskService.getTaskById(bearerToken,11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> taskService.getTaskById(bearerToken,11));
    }

    @Test
    void givenValidOrganizationId_whenGetAllTasks_thenReturnTaskResponseDtoList() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findAllByOrganizationId(anyInt())).thenReturn(taskList);
        when(taskResponseMapper.mapToTaskResponseDtoList(any())).thenReturn(taskResponseDtoList);

        // act
        List<TaskResponseDto> resultTaskList = taskService.getAllTasks(bearerToken);

        // assert
        assertThat(resultTaskList).isNotNull().hasSize(2);
        assertThat(resultTaskList.get(0).getTitle()).isEqualTo("AA");
        assertThat(resultTaskList.get(1).getTitle()).isEqualTo("CC");
    }

    @Test
    void givenInvalidOrganizationId_whenGetAllTasks_thenReturnEmptyList() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findAllByOrganizationId(anyInt())).thenReturn(Collections.emptyList());

        // act
        List<TaskResponseDto> resultTaskList = taskService.getAllTasks(bearerToken);

        // assert
        assertThat(resultTaskList).isEmpty();
    }

    @Test
    void givenValidOrganizationIdAndTaskId_whenDeleteTask_thenNothing() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.ofNullable(task));
        doNothing().when(taskRepository).deleteById(anyInt());

        // act
        taskService.deleteTask(bearerToken,1);

        // assert
        verify(taskRepository, times(1)).deleteById(1);
    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenDeleteTask_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> taskService.deleteTask(bearerToken,11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> taskService.deleteTask(bearerToken,11));
    }

    @Test
    void givenValidOrganizationIdAndTaskDto_whenSaveTask_thenReturnTaskResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskMapper.mapToTask(any())).thenReturn(task);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.of(organization));
        when(userRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.of(user));
        when(userRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.of(user2));

        when(taskRepository.save(any())).thenReturn(savedTask);

        doNothing().when(emailService).sendEmail(anyString(),anyString(),anyString());

        when(taskResponseMapper.mapToTaskResponseDto(any())).thenReturn(taskResponseDto);
        // act
        TaskResponseDto resultTaskResponseDto = taskService.saveTask(bearerToken,taskDto);

        // assert
        assertThat(resultTaskResponseDto).isNotNull();
        assertThat(resultTaskResponseDto.getTitle()).isEqualTo("AA");
        assertThat(resultTaskResponseDto.getDescription()).isEqualTo("BB");
        assertThat(resultTaskResponseDto.getAssignedUsers()).hasSize(2);

    }

    @Test
    void givenInvalidOrganizationIdAndValidTaskDto_whenSaveTask_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(organizationRepository.findById(anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> taskService.saveTask(bearerToken,taskDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> taskService.saveTask(bearerToken,taskDto));

    }

    @Test
    void givenValidOrganizationIdAndTaskIdAndTaskDto_whenUpdateTask_thenReturnTaskResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskMapper.mapToTask(any())).thenReturn(task);
        when(taskRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.of(taskDb));
        when(userRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.of(user));
        when(userRepository.findByIdAndOrganizationId(anyInt(),anyInt())).thenReturn(Optional.of(user2));

        when(taskRepository.save(any())).thenReturn(savedTask);
        when(taskResponseMapper.mapToTaskResponseDto(any())).thenReturn(taskResponseDto);
        // act
        TaskResponseDto resultTaskResponseDto = taskService.updateTask(bearerToken,1,taskDto);

        // assert
        assertThat(resultTaskResponseDto).isNotNull();
        assertThat(resultTaskResponseDto.getTitle()).isEqualTo("AA");
        assertThat(resultTaskResponseDto.getDescription()).isEqualTo("BB");
        assertThat(resultTaskResponseDto.getAssignedUsers()).hasSize(2);

    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenUpdateTask_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(taskRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> taskService.updateTask(bearerToken,11, taskDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> taskService.updateTask(bearerToken,11, taskDto));
    }

}
