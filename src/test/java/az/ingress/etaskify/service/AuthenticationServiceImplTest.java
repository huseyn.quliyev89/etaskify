package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.AuthRequestDto;
import az.ingress.etaskify.dto.JwtResponseDto;
import az.ingress.etaskify.dto.RefreshTokenRequestDto;
import az.ingress.etaskify.model.RefreshToken;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.security.JwtTokenProvider;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceImplTest {

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;
    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    @Mock
    private UserService userService;

    @Mock
    private RefreshTokenService refreshTokenService;


    private AuthRequestDto authRequestDto;

    private RefreshToken refreshToken;

    private User user;

    private JwtResponseDto jwtResponseDto;

    private String accessToken;

    private String token;

    private RefreshTokenRequestDto refreshTokenRequestDto;

    @BeforeEach
    void setUp() {
        authRequestDto = AuthRequestDto.builder()
                .username("pasha_admin")
                .password("123456Qh")
                .build();

        user = User.builder()
                .username("pasha_admin")
                .password("123456Qh")
                .build();

        accessToken = "eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwYXNoYV9hZG1pbiIsImlkIjoxLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIl0sImlhdCI6MTcwOTQ4ODQzNSwiZXhwIjoxNzA5NDg4NDk1fQ.TjLjs_Kee347qcsi7P3JJQdKNd2zafboFRSu5MUcjqO7MYdFFuk_67xnh7n04u95";

        token = "005deb6f-3940-44c9-b4c1-729ec67011f9";

        jwtResponseDto = JwtResponseDto.builder()
                .accessToken(accessToken)
                .token(token)
                .build();

        refreshToken = RefreshToken.builder()
                        .token(token)
                        .expiryDate(LocalDateTime.now())
                        .user(user)
                        .build();

        refreshTokenRequestDto = RefreshTokenRequestDto.builder().token(token).build();
    }

    @Test
    void givenValidLoginDto_whenLogin_thenReturnJwtAuthResponse() {
        // arrange
        when(userService.findByUsername(anyString())).thenReturn(user);
        when(refreshTokenService.createRefreshToken(anyString())).thenReturn(refreshToken);
        when(jwtTokenProvider.generateToken(any())).thenReturn(accessToken);

        // act
        JwtResponseDto response = authenticationService.login(authRequestDto);

        // assert
        assertThat(response).isNotNull();
        assertThat(response.getAccessToken()).isEqualTo(accessToken);
        assertThat(response.getToken()).isEqualTo(token);

    }

    @Test
    void givenValidHttpRequest_whenRefreshToken_thenReturnJwtAuthResponse() {
        // arrange
        when(refreshTokenService.findByToken(any())).thenReturn(Optional.ofNullable(refreshToken));
        when(refreshTokenService.verifyExpiration(any())).thenReturn(refreshToken);
        when(jwtTokenProvider.generateToken(any())).thenReturn(accessToken);

        // Act
        JwtResponseDto response = authenticationService.refreshToken(refreshTokenRequestDto);

        // Assert
        assertThat(response).isNotNull();
        assertThat(response.getAccessToken()).isEqualTo(accessToken);
        assertThat(response.getToken()).isEqualTo(token);
    }

}
