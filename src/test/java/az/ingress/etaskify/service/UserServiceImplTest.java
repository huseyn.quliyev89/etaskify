package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.*;
import az.ingress.etaskify.enums.TaskStatus;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.exception.UserNotDeletableException;
import az.ingress.etaskify.mapper.UserRequestMapper;
import az.ingress.etaskify.mapper.UserResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.Task;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.RoleRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserResponseMapper userResponseMapper;

    @Mock
    private UserRequestMapper userRequestMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    private User user;

    private User userNotAssignedTask;

    private User user2;

    private User userDb;

    private UserResponseDto userResponseDto;

    private UserResponseDto userResponseDto2;


    private List<User> userList;

    private List<UserResponseDto> userResponseDtoList;

    private List<User> userEmptyList;


    private UserResponseDto updatedUserResponseDto;

    private Organization organization;

    private RoleDto roleDto;
    private Role role;

    private User updatedUser;

    private UserRequestDto userRequestDto;

    private Task task;

    private List<Task> taskList;

    private String bearerToken;

    private Integer organizationId;


    @BeforeEach
    public void setUp() {
        organization = Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        role = Role.builder()
                .id(1)
                .name(UserRole.ROLE_ADMIN)
                .build();

        roleDto = RoleDto.builder()
                .name(UserRole.ROLE_ADMIN)
                .build();

        task = Task.builder()
                .id(1)
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organization)
                .assignedUsers(userList)
                .build();

        taskList = Arrays.asList(task);

        user = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .tasks(taskList)
                .build();

        user = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev").
                password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .tasks(taskList)
                .build();

        user2 = User.builder()
                .id(2)
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("mikayil.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        userDb = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        userList = Arrays.asList(user, user2);

        userNotAssignedTask = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .tasks(Collections.emptyList())
                .build();

        userResponseDto = UserResponseDto.builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .email("huseyn.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDto2 = UserResponseDto.builder()
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .email("mikayil.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDtoList = Arrays.asList(userResponseDto, userResponseDto2);

        userEmptyList = Collections.emptyList();

        updatedUserResponseDto = UserResponseDto
                .builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .email("hguliyev@gmail.com")
                .roles(List.of(roleDto))
                .build();

        updatedUser = User.builder().
                id(1).name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("hguliyev@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        userRequestDto = UserRequestDto.builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("hguliyev@gmail.com")
                .roles(List.of(roleDto))
                .build();

        bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

        organizationId = 1;

    }

    @Test
    void givenValidOrganizationIdAndUserid_whenGetUserById_thenReturnUserResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.ofNullable(user));
        when(userResponseMapper.mapToUserResponseDto(any())).thenReturn(userResponseDto);

        // act
        UserResponseDto returnUserResponseDto = userService.getUserById(bearerToken, 1);

        // assert
        assertThat(returnUserResponseDto).isNotNull();
        assertThat(returnUserResponseDto.getName()).isEqualTo("Huseyn");
        assertThat(returnUserResponseDto.getSurname()).isEqualTo("Guliyev");
        assertThat(returnUserResponseDto.getUsername()).isEqualTo("hguliyev");
        assertThat(returnUserResponseDto.getRoles()).containsExactly(roleDto);
        verify(userRepository, times(1)).findByIdAndOrganizationId(1, 1);
    }

    @Test
    void givenValidOrganizationIdInvalidUserId_whenGetOrganizationById_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> userService.getUserById(bearerToken, 11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> userService.getUserById(bearerToken, 11));
    }

    @Test
    void givenValidOrganizationId_whenGetAllUsers_thenReturnUserResponseDtoList() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findAllByOrganizationId(anyInt())).thenReturn(userList);
        when(userResponseMapper.mapToUserResponseDtoList(any())).thenReturn(userResponseDtoList);

        // act
        List<UserResponseDto> resultUserList = userService.getAllUsers(bearerToken);

        // assert
        assertThat(resultUserList).isNotNull().hasSize(2);
        assertThat(resultUserList.get(0).getName()).isEqualTo("Huseyn");
        assertThat(resultUserList.get(1).getName()).isEqualTo("Mikayil");
    }

    @Test
    void givenInvalidOrganizationId_whenGetAllUsers_thenReturnEmptyList() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findAllByOrganizationId(anyInt())).thenReturn(userEmptyList);

        // act
        List<UserResponseDto> resultOrganizationList = userService.getAllUsers(bearerToken);

        // assert
        assertThat(resultOrganizationList).isEmpty();
    }

    @Test
    void givenValidOrganizationIdAndUserId_whenDeleteUser_thenNothing() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.ofNullable(userNotAssignedTask));
        doNothing().when(userRepository).deleteById(anyInt());

        // act
        userService.deleteUser(bearerToken, 1);

        // assert
        verify(userRepository, times(1)).deleteById(1);
    }

    @Test
    void givenValidOrganizationIdAndInvalidUserId_whenDeleteUser_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> userService.deleteUser(bearerToken, 11)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> userService.deleteUser(bearerToken, 11));
    }

    @Test
    void givenValidOrganizationIdAndInvalidUserId_whenDeleteUser_thenReturnExceptionForAssignedTask() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.ofNullable(user));

        // act & assert
        assertThatThrownBy(() -> userService.deleteUser(bearerToken, 11)).isInstanceOf(UserNotDeletableException.class);
        assertThrows(UserNotDeletableException.class, () -> userService.deleteUser(bearerToken, 11));
    }


    @Test
    void givenValidOrganizationIdAndUserIdAndUserRequestDto_whenUpdateUser_thenReturnUserResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.of(userDb));
        when(userRequestMapper.mapToUser(any())).thenReturn(user);
        user.setId(user.getId());
        user.setOrganization(organization);
        user.setRoles(List.of(role));
        when(userRepository.save(any())).thenReturn(updatedUser);
        when(userResponseMapper.mapToUserResponseDto(any())).thenReturn(updatedUserResponseDto);

        // act
        UserResponseDto updatedUser = userService.updateUser(bearerToken, 1, userRequestDto);

        // assert
        assertThat(updatedUser.getEmail()).isEqualTo("hguliyev@gmail.com");
    }

    @Test
    void givenInvalidOrganizationIdAndInvalidUserIdValidUserRequestDto_whenUpdateUser_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByIdAndOrganizationId(anyInt(), anyInt())).thenReturn(Optional.empty());

        // act & assert
        assertThatThrownBy(() -> userService.updateUser(bearerToken, 11, userRequestDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> userService.updateUser(bearerToken, 11, userRequestDto));
    }

    @Test
    void givenValidOrganizationIdAndUserRequestDto_whenSaveUser_thenReturnUserResponseDto() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(roleRepository.findByName(any())).thenReturn(Optional.of(role));
        when(organizationRepository.findById(1)).thenReturn(Optional.of(organization));
        when(userRepository.save(any())).thenReturn(user);
        when(userResponseMapper.mapToUserResponseDto(any())).thenReturn(userResponseDto);

        // act
        UserResponseDto savedUser = userService.saveUser(bearerToken, userRequestDto);

        //
        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getName()).isEqualTo("Huseyn");
        assertThat(savedUser.getSurname()).isEqualTo("Guliyev");
        assertThat(savedUser.getUsername()).isEqualTo("hguliyev");
        assertThat(savedUser.getEmail()).isEqualTo("huseyn.quliyev89@gmail.com");
        assertThat(savedUser.getRoles()).isEqualTo(List.of(roleDto));
    }

    @Test
    void givenValidOrganizationIdAndInvalidUserRequestDto_whenSaveUser_thenReturnException() {
        // arrange
        when(jwtTokenProvider.getOrganizationId(anyString())).thenReturn(organizationId);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

        // act & assert
        assertThatThrownBy(() -> userService.saveUser(bearerToken, userRequestDto)).isInstanceOf(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> userService.saveUser(bearerToken, userRequestDto));
    }


}
