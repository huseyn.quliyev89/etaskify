package az.ingress.etaskify.controller;


import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.dto.SignUpRequestDto;
import az.ingress.etaskify.dto.SignUpResponseDto;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.security.JwtAuthenticationFilter;
import az.ingress.etaskify.security.JwtTokenProvider;
import az.ingress.etaskify.service.OrganizationService;
import az.ingress.etaskify.service.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(OrganizationController.class)
@RunWith(SpringRunner.class)
@WithMockUser(username = "hguliyev", roles = {"ADMIN"})
public class OrganizationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrganizationService organizationService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private JwtTokenProvider jwtTokenProvider;

    @MockBean
    private UserDetailsService userDetailsService;

    @Autowired
    private ObjectMapper objectMapper;
    private Organization organization;
    private OrganizationDto organizationDto;

    private List<Organization> organizationList;

    private List<OrganizationResponseDto> organizationResponseDtoList;

    private List<Organization> organizationEmptyList;


    private SignUpResponseDto signUpResponseDto;

    private Role role;

    private User user;

    private String bearerToken;



    @BeforeEach
    public void setUp() {

        signUpResponseDto = SignUpResponseDto.builder().
                name("PASHA Bank").
                phone("+994512296643").
                address("Neftchilar pr. 143").
                username("pasha_admin").
                email("huseyn.quliyev89@gmail.com").build();

        role = Role.builder().id(1)
                .name(UserRole.ROLE_ADMIN)
                .build();

        user = User.builder()
                .id(1)
                .username("pasha_admin")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com").roles(List.of(role))
                .organization(organization).build();


        organization = Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationDto = OrganizationDto.builder()
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationList = Arrays.asList(Organization.builder()
                        .id(1).name("PASHA Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143").
                        build(),
                Organization.builder()
                        .id(2)
                        .name("ABB Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build());

        organizationResponseDtoList = Arrays.asList(OrganizationResponseDto.builder()
                        .id(1).name("PASHA Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build(),
                OrganizationResponseDto.builder()
                        .id(2).name("ABB Bank")
                        .phone("+994512296643")
                        .address("Neftchilar pr. 143")
                        .build());

        organizationEmptyList = Collections.emptyList();

        bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

    }

    @Test
    void givenValidOrganizationId_whenGetOrganizationById_thenReturnOrganizationDto() throws Exception {
        // arrange
        when(organizationService.getOrganizationById(anyString())).thenReturn(organizationDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization", 1)
                        .header("Authorization", bearerToken)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("PASHA Bank"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").value("+994512296643"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value("Neftchilar pr. 143"));

    }

    @Test
    void givenInvalidOrganizationId_whenGetOrganizationById_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(organizationService).getOrganizationById(anyString());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization", 1)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

//    @Test
//    void givenEmptyParameter_whenGetAllOrganizations_thenReturnOrganizationResponseDtoList() throws Exception {
//        // arrange
//        when(organizationService.getAllOrganizations()).thenReturn(organizationResponseDtoList);
//
//        // act & assert
//        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization")).andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("PASHA Bank"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("ABB Bank"));
//    }

//    @Test
//    void givenEmptyParameter_whenGetAllOrganizations_thenReturnEmptyList() throws Exception {
//        // arrange
//        when(organizationService.getAllOrganizations()).thenReturn(Collections.emptyList());
//
//        // act & assert
//        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization"))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
//
//    }

    @Test
    void givenValidOrganizationIdAndValidOrganizationDto_whenUpdateOrganization_thenReturnOrganizationDto() throws Exception {
        when(organizationService.updateOrganization(anyString(), any())).thenReturn(organizationDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsString(organizationDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("PASHA Bank"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").value("+994512296643"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value("Neftchilar pr. 143"));

    }

    @Test
    void givenInvalidOrganizationIdAndValidOrganizationDto_whenUpdateOrganization_thenReturnException() throws Exception {
        // arrange

        doThrow(ResourceNotFoundException.class).when(organizationService).updateOrganization(anyString(), any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(organizationDto))).
                andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void givenValidOrganizationId_whenDeleteOrganization_thenNothing() throws Exception {
        // arrange
        doNothing().when(organizationService).deleteOrganization(anyString());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Test
    void givenInvalidOrganizationId_whenDeleteOrganization_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(organizationService).deleteOrganization(anyString());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnSignUpResponse() throws Exception {
        // arrange
        when(organizationService.saveOrganizationAndUser(any())).thenReturn(signUpResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization")
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpResponseDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("PASHA Bank"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").value("+994512296643"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value("Neftchilar pr. 143"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("pasha_admin"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.quliyev89@gmail.com"));
    }

    @Test
    void givenInValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnExistingUserException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(organizationService).saveOrganizationAndUser(any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization")
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void givenInValidSignUpRequest_whenSaveOrganizationAndUser_thenReturnExistingOrganizationException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(organizationService).saveOrganizationAndUser(any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization")
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

}
