package az.ingress.etaskify.controller;

import az.ingress.etaskify.dto.RoleDto;
import az.ingress.etaskify.dto.UserResponseDto;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.security.JwtTokenProvider;
import az.ingress.etaskify.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@WebMvcTest(UserController.class)
@RunWith(SpringRunner.class)
@WithMockUser(username = "hguliyev", roles = {"ADMIN"})
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private JwtTokenProvider jwtTokenProvider;

    @MockBean
    private UserDetailsService userDetailsService;

    @Autowired
    private ObjectMapper objectMapper;

    private UserResponseDto userResponseDto;

    private UserResponseDto userResponseDto2;

    private List<UserResponseDto> userResponseDtoList;

    private RoleDto roleDto;

    private String bearerToken;


    @BeforeEach
    public void setUp() {

        roleDto = RoleDto.builder()
                .name(UserRole.ROLE_ADMIN)
                .build();

        userResponseDto = UserResponseDto.builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .email("huseyn.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDto2 = UserResponseDto.builder()
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .email("mikayil.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDtoList = Arrays.asList(userResponseDto, userResponseDto2);

        bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

    }

    @Test
    void givenValidOrganizationIdAndUserid_whenGetUserById_thenReturnUserResponseDto() throws Exception {
        // arrange
        when(userService.getUserById(anyString(), anyInt())).thenReturn(userResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Huseyn"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("Guliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("hguliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.quliyev89@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles[0].name").value("ROLE_ADMIN"));
    }

    @Test
    void givenValidOrganizationIdInvalidUserId_whenGetOrganizationById_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(userService).getUserById(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidOrganizationId_whenGetAllUsers_thenReturnUserResponseDtoList() throws Exception {
        // arrange
        when(userService.getAllUsers(anyString())).thenReturn(userResponseDtoList);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/user", 1)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Huseyn"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Mikayil"));
    }

    @Test
    void givenInvalidOrganizationId_whenGetAllUsers_thenReturnEmptyList() throws Exception {
        // arrange
        when(userService.getAllUsers(anyString())).thenReturn(Collections.emptyList());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/user", 1)
                .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));

    }

    @Test
    void givenValidOrganizationIdAndUserId_whenDeleteUser_thenNothing() throws Exception {
        // arrange
        doNothing().when(userService).deleteUser(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(""));
    }

    @Test
    void givenValidOrganizationIdAndInvalidUserId_whenDeleteUser_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(userService).deleteUser(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidOrganizationIdAndUserIdAndUserRequestDto_whenUpdateUser_thenReturnUserResponseDto() throws Exception {
        // arrange
        when(userService.updateUser(anyString(), anyInt(), any())).thenReturn(userResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsString(userResponseDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Huseyn"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("Guliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("hguliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.quliyev89@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles[0].name").value("ROLE_ADMIN"));
    }

    @Test
    void givenInvalidOrganizationIdAndInvalidUserIdValidUserRequestDto_whenUpdateUser_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(userService).updateUser(anyString(), anyInt(), any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization/user/{userId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userResponseDto))).
                andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void givenValidOrganizationIdAndUserRequestDto_whenSaveUser_thenReturnUserResponseDto() throws Exception {
        // arrange
        when(userService.saveUser(anyString(), any())).thenReturn(userResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization/user", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userResponseDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Huseyn"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("Guliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("hguliyev"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("huseyn.quliyev89@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles[0].name").value("ROLE_ADMIN"));
    }

    @Test
    void givenValidOrganizationIdAndInvalidUserRequestDto_whenSaveUser_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(userService).saveUser(anyString(), any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization/user", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

}
