package az.ingress.etaskify.controller;


import az.ingress.etaskify.dto.*;
import az.ingress.etaskify.enums.TaskStatus;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.security.JwtAuthenticationFilter;
import az.ingress.etaskify.security.JwtTokenProvider;
import az.ingress.etaskify.service.TaskService;
import az.ingress.etaskify.service.TaskServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@WebMvcTest(TaskController.class)
@RunWith(SpringRunner.class)
@WithMockUser(username = "hguliyev", roles = {"ADMIN"})
public class TaskControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private JwtTokenProvider jwtTokenProvider;

    @MockBean
    private UserDetailsService userDetailsService;

    @Autowired
    private ObjectMapper objectMapper;

    private TaskDto taskDto;


    private TaskResponseDto taskResponseDto;

    private TaskResponseDto taskResponseDto2;

    private List<TaskResponseDto> taskResponseDtoList;

    private Organization organization;

    private OrganizationDto organizationDto;

    private User user;

    private User user2;

    private List<User> userList;

    private Role role;

    private RoleDto roleDto;

    private UserResponseDto userResponseDto;

    private UserResponseDto userResponseDto2;

    private List<UserResponseDto> userResponseDtoList;

    private String bearerToken;



    @BeforeEach
    public void setUp() {
        organization = Organization.builder()
                .id(1)
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        organizationDto = OrganizationDto.builder()
                .name("PASHA Bank")
                .phone("+994512296643")
                .address("Neftchilar pr. 143")
                .build();

        role = Role.builder()
                .id(1)
                .name(UserRole.ROLE_ADMIN)
                .build();

        roleDto = RoleDto.builder()
                .name(UserRole.ROLE_ADMIN)
                .build();

        user = User.builder()
                .id(1)
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("huseyn.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        user2 = User.builder()
                .id(2)
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .password(passwordEncoder.encode("123456"))
                .email("mikayil.quliyev89@gmail.com")
                .organization(organization)
                .roles(List.of(role))
                .build();

        userList = Arrays.asList(user, user2);

        userResponseDto = UserResponseDto.builder()
                .name("Huseyn")
                .surname("Guliyev")
                .username("hguliyev")
                .email("huseyn.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDto2 = UserResponseDto.builder()
                .name("Mikayil")
                .surname("Guliyev")
                .username("mguliyev")
                .email("mikayil.quliyev89@gmail.com")
                .roles(List.of(roleDto))
                .build();

        userResponseDtoList = Arrays.asList(userResponseDto, userResponseDto2);

        taskDto = TaskDto.builder()
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .assignedUsers(userList)
                .build();

        taskResponseDto = TaskResponseDto.builder()
                .title("AA")
                .description("BB")
                .deadline(LocalDate.of(2024, 2, 28))
                .status(TaskStatus.TODO)
                .organization(organizationDto)
                .assignedUsers(userResponseDtoList)
                .build();

        taskResponseDto2 = TaskResponseDto.builder()
                .title("CC")
                .description("DD")
                .deadline(LocalDate.of(2024, 3, 28))
                .status(TaskStatus.TODO)
                .organization(organizationDto)
                .assignedUsers(userResponseDtoList)
                .build();

        taskResponseDtoList = Arrays.asList(taskResponseDto, taskResponseDto2);

        bearerToken = "Bearer eyJ0b2tlblR5cGUiOiJBQ0NFU1MiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1bmlfYWRtaW4iLCJpZCI6OCwib3JnYW5pemF0aW9uSWQiOjUsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNzEyMjU4MTk0LCJleHAiOjE3MTIyNTg0OTR9.iOT8A_dOdZfawLHDR8Q1raaZOzOIv0B-4I5wq6pTksi8kAeCf5bQJYr9yhGIiQpm";

    }


    @Test
    void givenValidOrganizationIdAndTaskId_whenGetTaskById_thenReturnTaskResponseDto() throws Exception {
        // arrange
        when(taskService.getTaskById(anyString(), anyInt())).thenReturn(taskResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/task/{taskId}", 1, 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("BB"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.deadline").value("2024-02-28"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("TODO"));
    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenGetTaskById_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(taskService).getTaskById(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/task/{taskId}", 1, 1)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidOrganizationId_whenGetAllTasks_thenReturnTaskResponseDtoList() throws Exception {
        // arrange
        when(taskService.getAllTasks(anyString())).thenReturn(taskResponseDtoList);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/task", 1)
                        .header("Authorization", bearerToken)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value("AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].title").value("CC"));
    }

    @Test
    void givenInvalidOrganizationId_whenGetAllTasks_thenReturnEmptyList() throws Exception {
        // arrange
        when(taskService.getAllTasks(anyString())).thenReturn(Collections.emptyList());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/organization/task", 1)
                        .header("Authorization", bearerToken))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));

    }

    @Test
    void givenValidOrganizationIdAndTaskId_whenDeleteTask_thenNothing() throws Exception {
        // arrange
        doNothing().when(taskService).deleteTask(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization/task/{taskId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(""));

    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenDeleteTask_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(taskService).deleteTask(anyString(), anyInt());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/organization/task/{taskId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidOrganizationIdAndTaskDto_whenSaveTask_thenReturnTaskResponseDto() throws Exception {
        // arrange
        when(taskService.saveTask(anyString(), any())).thenReturn(taskResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization/task", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(taskResponseDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("BB"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.deadline").value("2024-02-28"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("TODO"));
    }

    @Test
    void givenInvalidOrganizationIdAndValidTaskDto_whenSaveTask_thenReturnException() throws Exception {
        // arrange
        doThrow(ResourceNotFoundException.class).when(taskService).saveTask(anyString(), any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/organization/task", 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

    }

    @Test
    void givenValidOrganizationIdAndTaskIdAndTaskDto_whenUpdateTask_thenReturnTaskResponseDto() throws Exception {
        when(taskService.updateTask(anyString(), anyInt(), any())).thenReturn(taskResponseDto);

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization/task/{taskId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsString(taskResponseDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("AA"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("BB"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.deadline").value("2024-02-28"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("TODO"));
    }

    @Test
    void givenValidOrganizationIdAndInvalidTaskId_whenUpdateTask_thenReturnException() throws Exception {
        // arrange

        doThrow(ResourceNotFoundException.class).when(taskService).updateTask(anyString(), anyInt(), any());

        // act & assert
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/organization/task/{taskId}", 1, 1)
                        .header("Authorization", bearerToken)
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(taskResponseDto))).
                andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

}




