package az.ingress.etaskify.enums;

public enum TaskStatus {
    TODO,
    IN_PROGRESS,
    DONE,
    CANCELLED,
    ON_HOLD;
}
