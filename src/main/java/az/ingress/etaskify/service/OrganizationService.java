package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.dto.SignUpRequestDto;
import az.ingress.etaskify.dto.SignUpResponseDto;

import java.util.List;

public interface OrganizationService {
    SignUpResponseDto saveOrganizationAndUser(SignUpRequestDto signUpRequestDto);
    OrganizationDto updateOrganization(String bearerToken, OrganizationDto organizationDto);
    //List<OrganizationResponseDto> getAllOrganizations();
    OrganizationDto getOrganizationById(String bearerToken);
    void deleteOrganization(String bearerToken);
}
