package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.*;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.exception.UserNotDeletableException;
import az.ingress.etaskify.mapper.UserRequestMapper;
import az.ingress.etaskify.mapper.UserResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.RoleRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserResponseMapper userResponseMapper;
    private final UserRequestMapper userRequestMapper;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponseDto saveUser(String bearerToken, UserRequestDto userRequestDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Optional<User> byUsername = userRepository.findByUsername(userRequestDto.getUsername());

        byUsername.ifPresent(userDb -> {
            throw new ResourceNotFoundException("User already exists with username: " + userRequestDto.getUsername());
        });

        List<Role> assignedRoles = userRequestDto.getRoles().stream()
                .map(roleDto -> roleRepository.findByName(roleDto.getName())
                        .orElseThrow(() -> new RuntimeException("Role not found: " + roleDto.getName())))
                .collect(Collectors.toList());

        Organization organizationDb = organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));

        User user = User.builder().
                name(userRequestDto.getName()).
                surname(userRequestDto.getSurname()).
                username(userRequestDto.getUsername()).
                password(passwordEncoder.encode(userRequestDto.getPassword())).
                email(userRequestDto.getEmail()).
                organization(organizationDb).
                roles(assignedRoles)
                .build();

        return userResponseMapper.mapToUserResponseDto(userRepository.save(user));
    }

    @Override
    public UserResponseDto updateUser(String bearerToken, Integer userId, UserRequestDto userRequestDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        User userDb = userRepository.findByIdAndOrganizationId(userId, organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found User with id = " + userId));
        User user = userRequestMapper.mapToUser(userRequestDto);
        user.setId(userId);
        user.setOrganization(userDb.getOrganization());
        user.setRoles(userDb.getRoles());
        User savedUser = userRepository.save(user);
        return userResponseMapper.mapToUserResponseDto(savedUser);
    }

    @Override
    public List<UserResponseDto> getAllUsers(String bearerToken) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        return userResponseMapper.mapToUserResponseDtoList(userRepository.findAllByOrganizationId(organizationId));
    }

    @Override
    public UserResponseDto getUserById(String bearerToken, Integer userId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        User userDb = userRepository.findByIdAndOrganizationId(userId, organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found User with id = " + userId));
        return userResponseMapper.mapToUserResponseDto(userDb);
    }

    @Override
    public void deleteUser(String bearerToken, Integer userId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        User userDb = userRepository.findByIdAndOrganizationId(userId, organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found User with id = " + userId));
        if(userDb.getTasks().isEmpty()) {
            userRepository.deleteById(userId);
        } else {
            throw new UserNotDeletableException("Cannot delete user with username = " + userDb.getUsername() + " as it is assigned to tasks.");
        }
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found for username: " + username));
    }
}
