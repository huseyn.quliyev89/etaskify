package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.UserRequestDto;
import az.ingress.etaskify.dto.UserResponseDto;
import az.ingress.etaskify.model.User;

import java.util.List;

public interface UserService {
    UserResponseDto saveUser(String bearerToken, UserRequestDto userRequestDto);
    UserResponseDto updateUser(String bearerToken, Integer userId, UserRequestDto userRequestDto);
    List<UserResponseDto> getAllUsers(String bearerToken);
    UserResponseDto getUserById(String bearerToken, Integer userId);
    void deleteUser(String bearerToken, Integer userId);

    User findByUsername(String username);


}
