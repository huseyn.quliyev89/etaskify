package az.ingress.etaskify.service;


import az.ingress.etaskify.dto.AuthRequestDto;
import az.ingress.etaskify.dto.JwtResponseDto;
import az.ingress.etaskify.dto.RefreshTokenRequestDto;
import org.springframework.web.bind.annotation.RequestBody;

public interface AuthenticationService {
    JwtResponseDto login(AuthRequestDto authRequestDto);
    JwtResponseDto refreshToken(RefreshTokenRequestDto refreshTokenRequestDto);



}
