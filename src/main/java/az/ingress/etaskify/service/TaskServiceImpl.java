package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.TaskDto;
import az.ingress.etaskify.dto.TaskResponseDto;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.mail.EmailService;
import az.ingress.etaskify.mapper.TaskMapper;
import az.ingress.etaskify.mapper.TaskResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Task;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.TaskRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService{
    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final TaskMapper taskMapper;
    private final TaskResponseMapper taskResponseMapper;
    private final EmailService emailService;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public TaskResponseDto saveTask(String bearerToken, TaskDto taskDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task task = taskMapper.mapToTask(taskDto);
        Organization organizationDb = organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));

        List<User> assignedUsers = taskDto.getAssignedUsers().stream()
                .map(userDto -> userRepository.findByIdAndOrganizationId(userDto.getId(), organizationId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found: " + userDto.getId())))
                .collect(Collectors.toList());

        task.setAssignedUsers(assignedUsers);
        task.setOrganization(organizationDb);
        TaskResponseDto responseDto = taskResponseMapper.mapToTaskResponseDto(taskRepository.save(task));

        for (User assignedUser:assignedUsers)
        {
            emailService.sendEmail(assignedUser.getEmail(),task.getTitle(),"Dear "+assignedUser.getName()+"\n" +
                    "\n" +
                    "I hope this email finds you well.\n" +
                    "\n" +
                    "I wanted to inform you that a new task has been assigned to you.");

        }
        return responseDto;

    }

    @Override
    public TaskResponseDto updateTask(String bearerToken, Integer taskId, TaskDto taskDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task task = taskMapper.mapToTask(taskDto);
        Task taskDb = taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));

        task.setId(taskId);
        task.setOrganization(taskDb.getOrganization());

        List<User> assignedUsers = taskDto.getAssignedUsers() != null ?
                taskDto.getAssignedUsers().stream()
                        .map(userDto -> userRepository.findByIdAndOrganizationId(userDto.getId(), organizationId)
                                .orElseThrow(() -> new ResourceNotFoundException("User not found: " + userDto.getId())))
                        .collect(Collectors.toList()) :
                taskRepository.findById(taskId)
                        .orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId))
                        .getAssignedUsers();

        task.setAssignedUsers(assignedUsers);

        return taskResponseMapper.mapToTaskResponseDto(taskRepository.save(task));
    }

    @Override
    public List<TaskResponseDto> getAllTasks(String bearerToken) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        return taskResponseMapper.mapToTaskResponseDtoList(taskRepository.findAllByOrganizationId(organizationId));
    }

    @Override
    public TaskResponseDto getTaskById(String bearerToken, Integer taskId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task taskDb = taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));
        return taskResponseMapper.mapToTaskResponseDto(taskDb);
    }

    @Override
    public void deleteTask(String bearerToken, Integer taskId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));
        taskRepository.deleteById(taskId);
    }
}
