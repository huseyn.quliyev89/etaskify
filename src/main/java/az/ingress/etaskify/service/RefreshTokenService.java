package az.ingress.etaskify.service;

import az.ingress.etaskify.model.RefreshToken;

import java.util.Optional;

public interface RefreshTokenService {
     RefreshToken createRefreshToken(String username);
     Optional<RefreshToken> findByToken(String token);
     RefreshToken verifyExpiration(RefreshToken token);
}
