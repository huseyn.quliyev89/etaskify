package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.dto.SignUpRequestDto;
import az.ingress.etaskify.dto.SignUpResponseDto;
import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.mapper.OrganizationMapper;
import az.ingress.etaskify.mapper.OrganizationResponseMapper;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.RoleRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtTokenProvider;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService{

    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final OrganizationMapper organizationMapper;
    private final OrganizationResponseMapper organizationResponseMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    @Transactional
    public SignUpResponseDto saveOrganizationAndUser(SignUpRequestDto signUpRequestDto) {

        Optional<User> byUsername = userRepository.findByUsername(signUpRequestDto.getUsername());

        Optional<Organization> byName = organizationRepository.findByName(signUpRequestDto.getName());

        byUsername.ifPresent(user -> {
            throw new ResourceNotFoundException("User already exists with username: " + signUpRequestDto.getUsername());
        });

        byName.ifPresent(organization -> {
            throw new ResourceNotFoundException("Organization already exists with name: " + signUpRequestDto.getName());
        });

        Optional<Role> role = roleRepository.findByName(UserRole.ROLE_ADMIN);

        Organization organization = Organization.builder().
                name(signUpRequestDto.getName()).
                phone(signUpRequestDto.getPhone()).
                address(signUpRequestDto.getAddress()).
                build();

        Organization savedOrganization = organizationRepository.save(organization);

        User user = User.builder().
                username(signUpRequestDto.getUsername()).
                password(passwordEncoder.encode(signUpRequestDto.getPassword())).
                email(signUpRequestDto.getEmail()).
                organization(savedOrganization).
                roles(List.of(role.get()))
                .build();

        User savedUser = userRepository.save(user);

        return SignUpResponseDto.builder().
                name(savedOrganization.getName()).
                phone(savedOrganization.getPhone()).
                address(savedOrganization.getAddress()).
                username(savedUser.getUsername()).
                email(savedUser.getEmail()).
                build();
    }

    @Override
    public OrganizationDto updateOrganization(String bearerToken, OrganizationDto organizationDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));
        Organization organization = organizationMapper.mapToOrganization(organizationDto);
        organization.setId(organizationId);
        Organization savedOrganization = organizationRepository.save(organization);
        return organizationMapper.mapToOrganizationDto(savedOrganization);
    }

//    @Override
//    public List<OrganizationResponseDto> getAllOrganizations() {
//        return organizationResponseMapper.mapToOrganizationResponseDtoList(organizationRepository.findAll());
//    }

    @Override
    public OrganizationDto getOrganizationById(String bearerToken) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Organization organizationDb = organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));
        return organizationMapper.mapToOrganizationDto(organizationDb);
    }

    @Override
    public void deleteOrganization(String bearerToken) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));
        organizationRepository.deleteById(organizationId);
    }
}
