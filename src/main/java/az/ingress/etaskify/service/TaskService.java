package az.ingress.etaskify.service;

import az.ingress.etaskify.dto.TaskDto;
import az.ingress.etaskify.dto.TaskResponseDto;

import java.util.List;

public interface TaskService {
    TaskResponseDto saveTask(String bearerToken, TaskDto taskDto);
    TaskResponseDto updateTask(String bearerToken, Integer taskId, TaskDto taskDto);
    List<TaskResponseDto> getAllTasks(String bearerToken);
    TaskResponseDto getTaskById(String bearerToken, Integer taskId);
    void deleteTask(String bearerToken, Integer taskId);
}
