package az.ingress.etaskify.service;


import az.ingress.etaskify.dto.AuthRequestDto;
import az.ingress.etaskify.dto.JwtResponseDto;
import az.ingress.etaskify.dto.RefreshTokenRequestDto;
import az.ingress.etaskify.exception.ResourceNotFoundException;
import az.ingress.etaskify.model.RefreshToken;
import az.ingress.etaskify.model.User;
import az.ingress.etaskify.security.JwtTokenProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final RefreshTokenService refreshTokenService;

    @Override
    public JwtResponseDto login(AuthRequestDto authRequestDto) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authRequestDto.getUsername(),
                authRequestDto.getPassword()
        ));

        User byUsername = userService.findByUsername(authRequestDto.getUsername());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(authRequestDto.getUsername());
        return JwtResponseDto.builder()
                .accessToken(jwtTokenProvider.generateToken(byUsername))
                .token(refreshToken.getToken()).build();

    }

    @Override
    public JwtResponseDto refreshToken(RefreshTokenRequestDto refreshTokenRequestDto) {
        return refreshTokenService.findByToken(refreshTokenRequestDto.getToken())
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String accessToken = jwtTokenProvider.generateToken(user);
                    return JwtResponseDto.builder()
                            .accessToken(accessToken)
                            .token(refreshTokenRequestDto.getToken()).build();
                }).orElseThrow(() ->new ResourceNotFoundException("Refresh Token is not in DB..!!"));

    }
}
