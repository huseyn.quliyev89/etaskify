package az.ingress.etaskify.mail;

public interface EmailService {
    void sendEmail(String to,
                   String subject,
                   String text);
}
