package az.ingress.etaskify.security;

import az.ingress.etaskify.dto.JwtResponseDto;
import az.ingress.etaskify.model.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    @Value("${security.jwt-secret}")
    private String jwtSecret;

    @Value("${security.jwt-expiration-milliseconds}")
    private long jwtExpirationDate;


    // generate JWT token
    public String generateToken(User user) {

        return
                Jwts.builder()
                        .setHeaderParam("tokenType", "ACCESS")
                        .subject(user.getUsername())
                        .claim("id", user.getId())
                        .claim("organizationId", user.getOrganization().getId())
                        .claim("authorities", user.getRoles().stream()
                                .map(role -> role.getName())
                                .collect(Collectors.toList())
                                .stream()
                                .map(Object::toString)
                                .toList())
                        .issuedAt(new Date())
                        .expiration(new Date(new Date().getTime() + jwtExpirationDate))
                        .signWith(key())
                        .compact();
    }


    private Key key() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
    }


    // get username from JWT token
    public String getUsername(String token) {

        return Jwts.parser()
                .verifyWith((SecretKey) key())
                .build()
                .parseSignedClaims(token)
                .getPayload()
                .getSubject();
    }

    public Integer getOrganizationId(String token) {
        return Jwts.parser()
                .verifyWith((SecretKey) key())
                .build()
                .parseClaimsJws(token)
                .getBody().get("organizationId", Integer.class);
    }

    public String getTokenType(String token) {
        Map<String, Object> header = Jwts.parser()
                .verifyWith((SecretKey) key())
                .build()
                .parseSignedClaims(token)
                .getHeader();
        return (String) header.get("tokenType");

    }

    // validate JWT token
    public boolean validateToken(String token) {
        try {
            Jwts.parser()
                    .verifyWith((SecretKey) key())
                    .build()
                    .parse(token);
            return true;

        } catch (MalformedJwtException e) {
            throw new MalformedJwtException("Invalid JWT token: " + e.getMessage());
        } catch (ExpiredJwtException e) {
            throw new ExpiredJwtException(null, null, "JWT token is expired: " + e.getMessage());
        } catch (UnsupportedJwtException e) {
            throw new UnsupportedJwtException("JWT token is unsupported: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("JWT claims string is empty: " + e.getMessage());
        } catch (SignatureException e) {
            throw new SignatureException("Invalid JWT token signature: " + e.getMessage());
        }

    }

}
