package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.model.Organization;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrganizationMapper {
    OrganizationDto mapToOrganizationDto(Organization organization);
    Organization mapToOrganization(OrganizationDto organizationDto);
    List<OrganizationDto> mapToOrganizationDtoList(List<Organization> organizationList);

}
