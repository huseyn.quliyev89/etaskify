package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.UserRequestDto;
import az.ingress.etaskify.dto.UserResponseDto;
import az.ingress.etaskify.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserRequestMapper {
    UserRequestDto mapToUserRequestDto(User user);
    User mapToUser(UserRequestDto userRequestDto);
    List<UserRequestDto> mapToUserRequestDtoList(List<User> userList);
}
