package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.model.Organization;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrganizationResponseMapper {
    OrganizationResponseDto mapToOrganizationResponseDto(Organization organization);
    Organization mapToOrganization(OrganizationResponseDto organizationResponseDto);
    List<OrganizationResponseDto> mapToOrganizationResponseDtoList(List<Organization> organizationList);

}
