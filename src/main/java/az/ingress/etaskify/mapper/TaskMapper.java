package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.TaskDto;
import az.ingress.etaskify.model.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskDto mapToTaskDto(Task task);
    Task mapToTask(TaskDto taskDto);
    List<TaskDto> mapToTaskDtoList(List<Task> taskList);
}
