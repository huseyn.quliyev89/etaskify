package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.TaskDto;
import az.ingress.etaskify.dto.TaskResponseDto;
import az.ingress.etaskify.model.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskResponseMapper {
    TaskResponseDto mapToTaskResponseDto(Task task);
    Task mapToTask(TaskResponseDto taskResponseDto);
    List<TaskResponseDto> mapToTaskResponseDtoList(List<Task> taskList);
}
