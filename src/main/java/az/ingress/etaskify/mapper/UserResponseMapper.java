package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.UserResponseDto;
import az.ingress.etaskify.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserResponseMapper {
    UserResponseDto mapToUserResponseDto(User user);
    User mapToUser(UserResponseDto userResponseDto);
    List<UserResponseDto> mapToUserResponseDtoList(List<User> userList);
}
