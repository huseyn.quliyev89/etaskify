package az.ingress.etaskify.controller;

import az.ingress.etaskify.aspect.LogExecutionTime;
import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.TaskDto;
import az.ingress.etaskify.dto.TaskResponseDto;
import az.ingress.etaskify.model.Task;
import az.ingress.etaskify.service.OrganizationService;
import az.ingress.etaskify.service.TaskService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/organization/task")
    @LogExecutionTime
    public TaskResponseDto saveTask(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid TaskDto taskDto) {
        return taskService.saveTask(bearerToken, taskDto);
    }

    @DeleteMapping("/organization/task/{taskId}")
    @LogExecutionTime
    public void deleteTask(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId) {
        taskService.deleteTask(bearerToken, taskId);
    }

    @GetMapping("/organization/task")
    @LogExecutionTime
    public List<TaskResponseDto> getAllTasks(@RequestHeader("Authorization") String bearerToken) {
        return taskService.getAllTasks(bearerToken);
    }

    @GetMapping("/organization/task/{taskId}")
    @LogExecutionTime
    public TaskResponseDto getTaskById(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId) {
        return taskService.getTaskById(bearerToken, taskId);
    }

    @PutMapping("/organization/task/{taskId}")
    @LogExecutionTime
    public TaskResponseDto updateTask(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId, @RequestBody @Valid TaskDto taskDto) {
        return taskService.updateTask(bearerToken, taskId, taskDto);
    }
}
