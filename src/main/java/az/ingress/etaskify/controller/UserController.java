package az.ingress.etaskify.controller;

import az.ingress.etaskify.aspect.LogExecutionTime;
import az.ingress.etaskify.dto.UserRequestDto;
import az.ingress.etaskify.dto.UserResponseDto;
import az.ingress.etaskify.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class UserController {
    private final UserService userService;

    @PostMapping("/organization/user")
    @LogExecutionTime
    public UserResponseDto saveUser(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid UserRequestDto userRequestDto) {
        return userService.saveUser(bearerToken, userRequestDto);
    }


    @PutMapping("/organization/user/{userId}")
    @LogExecutionTime
    public UserResponseDto updateUser(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId, @RequestBody @Valid UserRequestDto userRequestDto) {
        return userService.updateUser(bearerToken, userId, userRequestDto);
    }

    @GetMapping("/organization/user")
    @LogExecutionTime
    public List<UserResponseDto> getAllUsers(@RequestHeader("Authorization") String bearerToken) {
        return userService.getAllUsers(bearerToken);
    }

    @GetMapping("/organization/user/{userId}")
    @LogExecutionTime
    public UserResponseDto getUserById(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId) {
        return userService.getUserById(bearerToken, userId);
    }

    @DeleteMapping("/organization/user/{userId}")
    @LogExecutionTime
    public void deleteUser(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId) {
        userService.deleteUser(bearerToken, userId);
    }
}

