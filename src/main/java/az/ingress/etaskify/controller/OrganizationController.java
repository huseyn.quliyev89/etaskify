package az.ingress.etaskify.controller;

import az.ingress.etaskify.aspect.LogExecutionTime;
import az.ingress.etaskify.dto.OrganizationDto;
import az.ingress.etaskify.dto.OrganizationResponseDto;
import az.ingress.etaskify.dto.SignUpRequestDto;
import az.ingress.etaskify.dto.SignUpResponseDto;
import az.ingress.etaskify.service.OrganizationService;
import az.ingress.etaskify.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping("/organization")
    //@LogExecutionTime
    public SignUpResponseDto saveOrganizationAndUser(@RequestBody @Valid SignUpRequestDto signUpRequestDto) {
        return organizationService.saveOrganizationAndUser(signUpRequestDto);
    }

    @PutMapping("/organization")
    @LogExecutionTime
    public OrganizationDto updateOrganization(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid OrganizationDto organizationDto) {
        return organizationService.updateOrganization(bearerToken, organizationDto);
    }

//    @GetMapping("/organization")
//    @LogExecutionTime
//    public List<OrganizationResponseDto> getAllOrganizations() {
//        return organizationService.getAllOrganizations();
//    }

    @GetMapping("/organization")
    @LogExecutionTime
    public OrganizationDto getOrganizationById(@RequestHeader("Authorization") String bearerToken) {
        return organizationService.getOrganizationById(bearerToken);
    }

    @DeleteMapping("/organization")
    @LogExecutionTime
    public void deleteOrganization(@RequestHeader("Authorization") String bearerToken) {
        organizationService.deleteOrganization(bearerToken);
    }
}

