package az.ingress.etaskify.controller;

import az.ingress.etaskify.aspect.LogExecutionTime;
import az.ingress.etaskify.dto.AuthRequestDto;
import az.ingress.etaskify.dto.JwtResponseDto;
import az.ingress.etaskify.dto.RefreshTokenRequestDto;
import az.ingress.etaskify.model.RefreshToken;
import az.ingress.etaskify.service.AuthenticationService;
import az.ingress.etaskify.service.RefreshTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class LoginController {
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    @LogExecutionTime
    public JwtResponseDto login(@RequestBody AuthRequestDto authRequestDto) {
        return authenticationService.login(authRequestDto);
    }



    @PostMapping("/refreshToken")
    public JwtResponseDto refreshToken(@RequestBody RefreshTokenRequestDto refreshTokenRequestDto) {
        return authenticationService.refreshToken(refreshTokenRequestDto);
    }
}
