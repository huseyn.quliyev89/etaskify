package az.ingress.etaskify.exception;

import lombok.Getter;

@Getter
public enum ErrorCodes {
    RESOURCE_NOT_FOUND("eTaskify-EXCEPTION-001"),
    BAD_REQUEST("eTaskify-EXCEPTION-002"),
    USERNAME_ALREADY_EXIST("eTaskify-EXCEPTION-003"),
    USERNAME_NOT_FOUND("eTaskify-EXCEPTION-004"),
    NOT_AUTHORIZED("eTaskify-EXCEPTION-005"),
    DATA_INTEGRITY_VIOLATION("eTaskify-EXCEPTION-006"),
    USER_NOT_DELETABLE("eTaskify-EXCEPTION-007");


    final String code;
    ErrorCodes(String code) {
        this.code = code;
    }

}
