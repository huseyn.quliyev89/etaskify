package az.ingress.etaskify.exception;

public class UserNotDeletableException extends RuntimeException{
    public UserNotDeletableException(String msg) {
        super(msg);
    }
}
