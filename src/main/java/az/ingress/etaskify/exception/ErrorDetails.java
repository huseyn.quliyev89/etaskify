package az.ingress.etaskify.exception;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ErrorDetails {
     Date timestamp;
     Integer status;
     String code;
     String message;
     String description;
}
