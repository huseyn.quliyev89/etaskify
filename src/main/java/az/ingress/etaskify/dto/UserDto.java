package az.ingress.etaskify.dto;

import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    String name;
    String surname;
    String username;
    String password;
    String email;
    @JsonIgnore
    Organization organization;

    @JsonIgnore
    List<Role> roles;

    @JsonIgnore
    List<Task> tasks;
}
