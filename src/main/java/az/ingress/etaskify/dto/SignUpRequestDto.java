package az.ingress.etaskify.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SignUpRequestDto {
    @NotBlank
    String name;
    @NotBlank
    String phone;
    @NotBlank
    String address;
    @NotBlank
    String username;
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z]).{6,}$")
    String password;
    @Email
    String email;
}
