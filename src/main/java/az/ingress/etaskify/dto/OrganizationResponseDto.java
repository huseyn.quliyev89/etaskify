package az.ingress.etaskify.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponseDto {
    Integer id;
    String name;
    String phone;
    String address;
    //@JsonIgnore
    //List<UserDto> userList;
}
