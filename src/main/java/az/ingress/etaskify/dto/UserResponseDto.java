package az.ingress.etaskify.dto;

import az.ingress.etaskify.enums.UserRole;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponseDto {
    Integer id;
    String name;
    String surname;
    String username;
    String email;
    List<RoleDto> roles;
}
