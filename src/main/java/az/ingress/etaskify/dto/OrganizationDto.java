package az.ingress.etaskify.dto;

import az.ingress.etaskify.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationDto {
    @NotBlank
    String name;
    @NotBlank
    String phone;
    @NotBlank
    String address;
    //@JsonIgnore
    //List<UserDto> userList;
}
