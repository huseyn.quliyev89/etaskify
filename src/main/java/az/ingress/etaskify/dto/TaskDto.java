package az.ingress.etaskify.dto;

import az.ingress.etaskify.enums.TaskStatus;
import az.ingress.etaskify.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToMany;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskDto {
    @NotBlank
    String title;
    @NotBlank
    String description;
    @NotNull
    LocalDate deadline;
    @Enumerated(EnumType.STRING)
    @NotNull
    TaskStatus status;
    @Valid
    List<User> assignedUsers;
}
