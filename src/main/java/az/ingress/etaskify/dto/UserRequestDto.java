package az.ingress.etaskify.dto;

import az.ingress.etaskify.enums.UserRole;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDto {
    @NotBlank
    String name;
    @NotBlank
    String surname;
    @NotBlank
    String username;
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z]).{6,}$")
    String password;
    @Email
    String email;
    @NotNull
    List<RoleDto> roles;
}
