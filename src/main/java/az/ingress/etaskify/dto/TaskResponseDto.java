package az.ingress.etaskify.dto;

import az.ingress.etaskify.enums.TaskStatus;
import az.ingress.etaskify.model.Organization;
import az.ingress.etaskify.model.User;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponseDto {
    Integer id;
    String title;
    String description;
    LocalDate deadline;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    List<UserResponseDto> assignedUsers;
    OrganizationDto organization;
}
