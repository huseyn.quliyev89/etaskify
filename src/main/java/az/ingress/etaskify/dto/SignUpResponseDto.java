package az.ingress.etaskify.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SignUpResponseDto {
    String name;
    String phone;
    String address;
    String username;
    String email;
}
