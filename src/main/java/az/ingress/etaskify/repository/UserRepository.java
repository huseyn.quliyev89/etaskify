package az.ingress.etaskify.repository;

import az.ingress.etaskify.enums.UserRole;
import az.ingress.etaskify.model.Role;
import az.ingress.etaskify.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "select u from User u join fetch u.organization o join fetch u.roles r left join u.tasks t where u.id=:userId and u.organization.id=:organizationId")
    Optional<User> findByIdAndOrganizationId(Integer userId, Integer organizationId);

    @Query(value = "select u from User u join fetch u.organization o join fetch u.roles r left join u.tasks t where u.organization.id=:organizationId")
    List<User> findAllByOrganizationId(Integer organizationId);

    @Query("SELECT u FROM User u JOIN FETCH u.organization o JOIN FETCH u.roles r left join u.tasks t WHERE u.username = :username")
    Optional<User> findByUsername(String username);
}

