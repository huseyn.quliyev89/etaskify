FROM openjdk:17
COPY build/libs/*.jar /app/
WORKDIR /app/
RUN mv /app/*.jar /app/etaskify.jar
ENTRYPOINT ["java"]
CMD ["-jar","/app/etaskify.jar"]
